-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2016 at 03:32 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `khairul`
--

-- --------------------------------------------------------

--
-- Table structure for table `softdelete`
--

CREATE TABLE `softdelete` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `name` varchar(256) NOT NULL,
  `mobile` varchar(256) NOT NULL,
  `is_delete` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `softdelete`
--

INSERT INTO `softdelete` (`id`, `unique_id`, `name`, `mobile`, `is_delete`) VALUES
(4, '579365ab62220', 'Bangladesh', 'walton', 0),
(5, '579369c39929b', 'Mita', 'sumsang', 0),
(6, '57936db55c4b1', 'sumon', 'symphony', 0),
(7, '57936dba1f9c1', 'Khairul Islam', 'lenevo', 0),
(8, '57936dcd52ce6', 'Khairul Islam Hoccena kno', 'iphpone', 0),
(11, '5793715dc19eb', 'Khairul Islam', '01784983612', 0),
(12, '5793719236533', 'asdfasd', 'asdfad', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `softdelete`
--
ALTER TABLE `softdelete`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `softdelete`
--
ALTER TABLE `softdelete`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
