<?php

namespace Deleteapps;
use PDO;
class softdelete {
    
    public $unique_id='';
    public $title='';
    public $mobile='';
    public $user='root';
    public $password='';
    public $connection='';
    public $data='';
    public $is_delete='';
    public $time='';


    public function __construct() {
        session_start();
        $this->connection = new PDO('mysql:host=localhost;dbname=khairul', $this->user, $this->password);
        
    }
    
     public function prepare($data='')
    {
//        echo '<pre>';
//        print_r($data);
        
        if(!empty($data['name']))
        {
            $this->title=$data['name'];
        }else{
            $_SESSION['emty_msg']='<b style=" color:red; font-size: 16px; ">Name is required Field..!!</b>';
        }
        
        if(!empty($data['mobile']))
        {
            $this->mobile=$data['mobile'];
        }else{
            $_SESSION['emty_msg']='<b style=" color:red; font-size: 16px; ">mobile is required Field..!!</b>';
        }
        if(array_key_exists('unique_id', $data))
        {
            $this->unique_id=$data['unique_id'];
        }
       
        return $this;
    }
    
     public function index()
    {
       $qry="SELECT * FROM softdelete WHERE is_delete=0 ORDER BY time DESC";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $table=$stmt->fetchAll();
       return $table;
    }
    
    public function store()
    {
        if(!empty($this->title)&& !empty($this->mobile))
        {
            date_default_timezone_set("Asia/Dhaka");
    $time= date("h:i:sa");
            try {
            $query = "INSERT INTO  softdelete(id, unique_id, name, mobile, is_delete, time)
                VALUES(:ide, :uni, :name, :mob, :isdele, :time )";
               $stmt=  $this->connection->prepare($query);
               $result=$stmt->execute(array(
                ':ide' => Null,
                ':uni' => uniqid(),
                ':name' => $this->title,
                ':mob' => $this->mobile,
                ':isdele' => '0',
                ':time'=>$time
                   
                   ));
             if($result)
             {
                $_SESSION['store_msg']='<b style=" color: blue;">Data Save Successful</b>';
                header('location:create.php');   
             }
            } catch (Exception $ex) {
                
            }
        }  else {
            header('location:create.php');  
        }
    }
    
    public function trush()
    {
     date_default_timezone_set("Asia/Dhaka");
    $time= date("h:i:sa");
        try {
              $sql = "UPDATE `softdelete` SET `is_delete` = '1', `time` ='$time'  WHERE `softdelete`.`unique_id`="."'".$this->unique_id."'";
           
                $stmt = $this->connection->prepare($sql);
                $result=$stmt->execute();
                if($result)
                {
                $_SESSION['store_msg']='<b style=" color: green;">Dalete Successful</b>';
                header("location:index.php");
                }
            
        } catch (Exception $ex) {
            
        }
    }
    
     public function deleteitem()
    {
       $qry="SELECT * FROM softdelete WHERE is_delete=1 ORDER BY time DESC";
       $stmt=  $this->connection->query($qry);
       $stmt->execute();
       $table=$stmt->fetchAll();
       return $table;
    }
    
     public function restore()
    {
     date_default_timezone_set("Asia/Dhaka");
    $time= date("h:i:sa");
        try {
              $sql = "UPDATE `softdelete` SET `is_delete` = '0',`time` ='$time' WHERE `softdelete`.`unique_id`="."'".$this->unique_id."'";
              
                $stmt = $this->connection->prepare($sql);
                $result=$stmt->execute();
                if($result)
                {
                $_SESSION['store_msg']='<b style=" color: green;">Restore Successful</b>';
                header("location:deleteitem.php");
                }
            
        } catch (Exception $ex) {
            
        }
    }
    
     public function delete()
    {
        try {
              $query = "DELETE FROM `softdelete` WHERE `softdelete`.`unique_id` =" . "'" . $this->unique_id . "'";
//              echo $query;
//              die();
              $stmt = $this->connection->prepare($query);
                if($stmt->execute())
                {
                $_SESSION['store_msg']='<b style=" color: red;">Data Delete Successful</b>';
                header("location:deleteitem.php");
                }    
        } catch (Exception $ex) {
            
        }
    }
    
    
}
